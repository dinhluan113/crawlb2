﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrawlB2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //Chương 1: Khái niệm và quy tắc giao thông đường bộ
            //Gồm 166 câu, từ câu 1 đến câu 166, trong đó có 45 câu điểm liệt.
            var urlChuong_1_1 = "http://daotaolaixehcm.vn/thi-bang-lai-xe/quy-tac-giao-thong-duong-bo-p1/";
            var urlChuong_1_2 = "http://daotaolaixehcm.vn/thi-bang-lai-xe/quy-tac-giao-thong-duong-bo-p2/";
            var urlChuong_1_3 = "http://daotaolaixehcm.vn/thi-bang-lai-xe/quy-tac-giao-thong-duong-bo-p3/";
            var urlChuong_1_4 = "http://daotaolaixehcm.vn/thi-bang-lai-xe/quy-tac-giao-thong-duong-bo-p4/";
            var urlChuong_1_5 = "http://daotaolaixehcm.vn/thi-bang-lai-xe/quy-tac-giao-thong-duong-bo-p5/";
            var urlChuong_1_6 = "http://daotaolaixehcm.vn/thi-bang-lai-xe/quy-tac-giao-thong-duong-bo-p6/";
            var urlChuong_1_7 = "http://daotaolaixehcm.vn/thi-bang-lai-xe/quy-tac-giao-thong-duong-bo-p7/";
            var urlChuong_1_8 = "http://daotaolaixehcm.vn/thi-bang-lai-xe/quy-tac-giao-thong-duong-bo-p8/";
            HtmlWeb web = new HtmlWeb();
            HtmlDocument document = new HtmlDocument();
            document = web.Load("http://daotaolaixehcm.vn/thi-bang-lai-xe/cac-khai-niem-p1/");

            var divContent = document.DocumentNode.SelectNodes("//div[contains(@class,'entry-content')]").FirstOrDefault();

            var childNodes = divContent.ChildNodes.Where(c => c.Name != "div").FirstOrDefault();
            var lstQuestionModel = new List<QuestionModel>();
            HtmlNode sibling = childNodes.NextSibling;
            while (sibling != null)
            {
                if (sibling.NodeType == HtmlNodeType.Element)
                    Console.WriteLine(sibling.OuterHtml);

                var model = new QuestionModel();
                if (sibling.Name == "h2")
                {
                    var ques = sibling.InnerText;
                    ques = ques.Substring(ques.IndexOf(": ") + 2, ques.Length - ques.IndexOf(": ") - 2);
                    if (ques == "0")
                        continue;
                    model.Question = ques;
                }

                if (sibling.Name == "ol")
                {
                    var childAns = sibling.ChildNodes.FirstOrDefault();
                    HtmlNode siblingAns = childAns.NextSibling;
                    while (siblingAns != null)
                    {
                        model.Answer.Add(siblingAns.InnerText);
                    }
                }
                //var objAnswer = lstAnswer[idx].ChildNodes.Where(c => c.Name == "li").ToList();
                //var numberIfAnswer = objAnswer.Count();
                //if (numberIfAnswer == 2)
                //{
                //    model.Answer1 = objAnswer[0].InnerText;
                //    model.Answer2 = objAnswer[1].ChildNodes.FirstOrDefault().InnerText;
                //}
                //else if (numberIfAnswer == 3)
                //{
                //    model.Answer1 = objAnswer[0].ChildNodes.FirstOrDefault().InnerText;
                //    model.Answer2 = objAnswer[1].ChildNodes.FirstOrDefault().InnerText;
                //    model.Answer3 = objAnswer[2].ChildNodes.FirstOrDefault().InnerText;
                //}
                //else if (numberIfAnswer == 4)
                //{
                //    model.Answer1 = objAnswer[0].ChildNodes.FirstOrDefault().InnerText;
                //    model.Answer2 = objAnswer[1].ChildNodes.FirstOrDefault().InnerText;
                //    model.Answer3 = objAnswer[2].ChildNodes.FirstOrDefault().InnerText;
                //    model.Answer4 = objAnswer[3].ChildNodes.FirstOrDefault().InnerText;
                //}
                //else if (numberIfAnswer == 5)
                //{
                //    model.Answer1 = objAnswer[0].ChildNodes.FirstOrDefault().InnerText;
                //    model.Answer2 = objAnswer[1].ChildNodes.FirstOrDefault().InnerText;
                //    model.Answer3 = objAnswer[2].ChildNodes.FirstOrDefault().InnerText;
                //    model.Answer4 = objAnswer[3].ChildNodes.FirstOrDefault().InnerText;
                //    model.Answer5 = objAnswer[4].ChildNodes.FirstOrDefault().InnerText;
                //}

                //var trueAns = lstTrueAnswer[idx].InnerText;
                //model.TrueAnswer = int.Parse(trueAns.Substring(6, trueAns.Length - 6).Trim());
                //idx++;

                lstQuestionModel.Add(model);
                sibling = sibling.NextSibling;
            }

            return View(lstQuestionModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }

    public class QuestionModel
    {
        public QuestionModel()
        {
            Answer = new List<string>();
        }
        public string Question { get; set; }
        public List<string> Answer { get; set; }
        public int TrueAnswer { get; set; }
    }
}